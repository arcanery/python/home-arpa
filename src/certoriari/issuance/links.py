# `certoriari` - Certificate Authority manager for home networks
# Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from dataclasses import dataclass

from cryptography import x509

from certoriari.issuance.keys import PrivateKey


@dataclass
class NodeInfo:
    name: x509.Name
    private_key: PrivateKey
    subject_alternative_names: list[x509.GeneralName]


class RootNodeInfo(NodeInfo):
    pass


@dataclass
class ChildNodeInfo(NodeInfo):
    issuer: NodeInfo
