# `certoriari` - Certificate Authority manager for home networks
# Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import annotations

import ipaddress
from abc import ABCMeta, abstractmethod
from dataclasses import dataclass
from urllib.parse import urlparse

from cryptography import x509


def to_a_label(v: str) -> str:
    return "@".join(
        ".".join(map(lambda s: s.encode("idna").decode(), part.split(".")))
        for part in v.split("@")
    )


class GeneralName(metaclass=ABCMeta):
    @property
    @abstractmethod
    def x509_general_name(self) -> x509.GeneralName: ...


@dataclass
class StrGeneralName(GeneralName):
    value: str


class RFC822Name(StrGeneralName):
    @property
    def x509_general_name(self) -> x509.RFC822Name:
        return x509.RFC822Name(to_a_label(self.value))


class DNSName(StrGeneralName):
    @property
    def x509_general_name(self) -> x509.DNSName:
        return x509.DNSName(to_a_label(self.value))


class UniformResourceIdentifier(StrGeneralName):
    @property
    def x509_general_name(self) -> x509.UniformResourceIdentifier:
        parsed = urlparse(self.value)

        if parsed.netloc:
            parsed = parsed._replace(netloc=to_a_label(parsed.netloc))
        if parsed.path:
            parsed = parsed._replace(path=to_a_label(parsed.path))

        return x509.UniformResourceIdentifier(parsed.geturl())


@dataclass
class DirectoryName(GeneralName):
    name: x509.Name

    @property
    def x509_general_name(self) -> x509.DirectoryName:
        return x509.DirectoryName(self.name)


@dataclass
class RegisteredID(GeneralName):
    oid: x509.ObjectIdentifier

    @property
    def x509_general_name(self) -> x509.RegisteredID:
        return x509.RegisteredID(self.oid)


IPAddressT = (
    ipaddress.IPv4Address
    | ipaddress.IPv6Address
    | ipaddress.IPv4Network
    | ipaddress.IPv6Network
)


@dataclass
class IPAddress(GeneralName):
    address: IPAddressT

    @property
    def x509_general_name(self) -> x509.IPAddress:
        return x509.IPAddress(self.address)


@dataclass
class OtherName(GeneralName):
    oid: x509.ObjectIdentifier
    value: bytes

    @property
    def x509_general_name(self) -> x509.OtherName:
        return x509.OtherName(self.oid, self.value)
