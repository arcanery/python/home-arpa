# `certoriari` - Certificate Authority manager for home networks
# Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import datetime

from cryptography import x509
from cryptography.hazmat.primitives import hashes

from certoriari.issuance.links import ChildNodeInfo, NodeInfo

SignatureHashAlgorithm = hashes.SHA256 | hashes.SHA512


def make_root_ca_certificate(
    node: NodeInfo,
    valid_days: int,
    signature_hash_algorithm: SignatureHashAlgorithm = hashes.SHA512(),
) -> x509.Certificate:
    builder = (
        x509.CertificateBuilder()
        .issuer_name(node.name)
        .not_valid_before(now := datetime.datetime.now(datetime.UTC))
        .not_valid_after(now + datetime.timedelta(days=valid_days))
        .serial_number(x509.random_serial_number())
        .subject_name(node.name)
        .public_key(public_key := node.private_key.public_key())
        .add_extension(x509.BasicConstraints(ca=True, path_length=None), critical=True)
        .add_extension(
            x509.KeyUsage(
                digital_signature=True,
                content_commitment=True,
                key_encipherment=False,
                data_encipherment=False,
                key_agreement=False,
                key_cert_sign=True,
                crl_sign=False,
                encipher_only=False,
                decipher_only=False,
            ),
            critical=True,
        )
        .add_extension(
            x509.SubjectKeyIdentifier.from_public_key(public_key),
            critical=False,
        )
        .add_extension(
            x509.AuthorityKeyIdentifier.from_issuer_public_key(public_key),
            critical=False,
        )
    )

    if node.subject_alternative_names:
        builder = builder.add_extension(
            x509.SubjectAlternativeName(node.subject_alternative_names),
            critical=False,
        )

    return builder.sign(
        private_key=node.private_key, algorithm=signature_hash_algorithm
    )


def make_intermediary_ca_certificate(
    node: ChildNodeInfo,
    valid_days: int,
    signature_hash_algorithm: SignatureHashAlgorithm = hashes.SHA512(),
    ocsp_url: str | None = None,
    path_length: int | None = None,
) -> x509.Certificate:
    builder = (
        x509.CertificateBuilder()
        .subject_name(node.name)
        .issuer_name(node.issuer.name)
        .not_valid_before(now := datetime.datetime.now(datetime.UTC))
        .not_valid_after(now + datetime.timedelta(days=valid_days))
        .serial_number(x509.random_serial_number())
        .public_key(public_key := node.private_key.public_key())
        .add_extension(
            x509.BasicConstraints(ca=True, path_length=path_length), critical=True
        )
        .add_extension(
            x509.KeyUsage(
                digital_signature=True,
                content_commitment=True,
                key_encipherment=False,
                data_encipherment=False,
                key_agreement=False,
                key_cert_sign=True,
                crl_sign=False,
                encipher_only=False,
                decipher_only=False,
            ),
            critical=True,
        )
        .add_extension(
            x509.SubjectKeyIdentifier.from_public_key(public_key),
            critical=False,
        )
        .add_extension(
            x509.AuthorityKeyIdentifier.from_issuer_public_key(
                node.issuer.private_key.public_key()
            ),
            critical=False,
        )
    )

    if node.subject_alternative_names:
        builder = builder.add_extension(
            x509.SubjectAlternativeName(node.subject_alternative_names),
            critical=False,
        )

    if node.issuer.subject_alternative_names:
        builder = builder.add_extension(
            x509.IssuerAlternativeName(node.issuer.subject_alternative_names),
            critical=False,
        )

    if ocsp_url:
        builder = builder.add_extension(
            x509.AuthorityInformationAccess(
                (
                    x509.AccessDescription(
                        access_method=x509.OID_OCSP,
                        access_location=x509.UniformResourceIdentifier(ocsp_url),
                    ),
                )
            ),
            critical=False,
        )

    return builder.sign(
        private_key=node.issuer.private_key, algorithm=signature_hash_algorithm
    )


def make_ocsp_signer_certificate(
    node: ChildNodeInfo,
    valid_days: int,
    signature_hash_algorithm: SignatureHashAlgorithm = hashes.SHA512(),
) -> x509.Certificate:
    builder = (
        x509.CertificateBuilder()
        .subject_name(node.name)
        .issuer_name(node.issuer.name)
        .not_valid_before(now := datetime.datetime.now(datetime.UTC))
        .not_valid_after(now + datetime.timedelta(days=valid_days))
        .serial_number(x509.random_serial_number())
        .public_key(public_key := node.private_key.public_key())
        .add_extension(x509.BasicConstraints(ca=True, path_length=0), critical=True)
        .add_extension(
            x509.KeyUsage(
                digital_signature=True,
                content_commitment=True,
                key_encipherment=False,
                data_encipherment=False,
                key_agreement=False,
                key_cert_sign=False,
                crl_sign=False,
                encipher_only=False,
                decipher_only=False,
            ),
            critical=True,
        )
        .add_extension(
            x509.ExtendedKeyUsage(
                (x509.OID_SERVER_AUTH, x509.OID_OCSP_SIGNING, x509.OID_TIME_STAMPING)
            ),
            critical=True,
        )
        .add_extension(x509.OCSPNoCheck(), critical=False)
        .add_extension(
            x509.SubjectKeyIdentifier.from_public_key(public_key),
            critical=False,
        )
        .add_extension(
            x509.AuthorityKeyIdentifier.from_issuer_public_key(
                node.issuer.private_key.public_key()
            ),
            critical=False,
        )
    )

    if node.subject_alternative_names:
        builder = builder.add_extension(
            x509.SubjectAlternativeName(node.subject_alternative_names),
            critical=False,
        )

    if node.issuer.subject_alternative_names:
        builder = builder.add_extension(
            x509.IssuerAlternativeName(node.issuer.subject_alternative_names),
            critical=False,
        )

    return builder.sign(
        private_key=node.issuer.private_key, algorithm=signature_hash_algorithm
    )


def make_crl_signer_certificate(
    node: ChildNodeInfo,
    valid_days: int,
    signature_hash_algorithm: SignatureHashAlgorithm = hashes.SHA512(),
) -> x509.Certificate:
    builder = (
        x509.CertificateBuilder()
        .subject_name(node.name)
        .issuer_name(node.issuer.name)
        .not_valid_before(now := datetime.datetime.now(datetime.UTC))
        .not_valid_after(now + datetime.timedelta(days=valid_days))
        .serial_number(x509.random_serial_number())
        .public_key(public_key := node.private_key.public_key())
        .add_extension(x509.BasicConstraints(ca=True, path_length=0), critical=True)
        .add_extension(
            x509.KeyUsage(
                digital_signature=True,
                content_commitment=True,
                key_encipherment=False,
                data_encipherment=False,
                key_agreement=False,
                key_cert_sign=False,
                crl_sign=True,
                encipher_only=False,
                decipher_only=False,
            ),
            critical=True,
        )
        .add_extension(
            x509.ExtendedKeyUsage((x509.OID_TIME_STAMPING,)),
            critical=True,
        )
        .add_extension(
            x509.SubjectKeyIdentifier.from_public_key(public_key),
            critical=False,
        )
        .add_extension(
            x509.AuthorityKeyIdentifier.from_issuer_public_key(
                node.issuer.private_key.public_key()
            ),
            critical=False,
        )
    )

    if node.subject_alternative_names:
        builder = builder.add_extension(
            x509.SubjectAlternativeName(node.subject_alternative_names),
            critical=False,
        )

    if node.issuer.subject_alternative_names:
        builder = builder.add_extension(
            x509.IssuerAlternativeName(node.issuer.subject_alternative_names),
            critical=False,
        )

    return builder.sign(
        private_key=node.issuer.private_key, algorithm=signature_hash_algorithm
    )


def make_server_end_entity_certificate(
    node: ChildNodeInfo,
    valid_days: int,
    signature_hash_algorithm: SignatureHashAlgorithm = hashes.SHA512(),
    ocsp_url: str | None = None,
) -> x509.Certificate:
    builder = (
        x509.CertificateBuilder()
        .subject_name(node.name)
        .issuer_name(node.issuer.name)
        .not_valid_before(now := datetime.datetime.now(datetime.UTC))
        .not_valid_after(now + datetime.timedelta(days=valid_days))
        .serial_number(x509.random_serial_number())
        .public_key(public_key := node.private_key.public_key())
        .add_extension(x509.BasicConstraints(ca=False, path_length=None), critical=True)
        .add_extension(
            x509.KeyUsage(
                digital_signature=True,
                content_commitment=True,
                key_encipherment=True,
                data_encipherment=False,
                key_agreement=True,
                key_cert_sign=False,
                crl_sign=False,
                encipher_only=False,
                decipher_only=False,
            ),
            critical=True,
        )
        .add_extension(x509.ExtendedKeyUsage((x509.OID_SERVER_AUTH,)), critical=True)
        .add_extension(
            x509.SubjectKeyIdentifier.from_public_key(public_key),
            critical=False,
        )
        .add_extension(
            x509.AuthorityKeyIdentifier.from_issuer_public_key(
                node.issuer.private_key.public_key()
            ),
            critical=False,
        )
    )

    if node.subject_alternative_names:
        builder = builder.add_extension(
            x509.SubjectAlternativeName(node.subject_alternative_names),
            critical=False,
        )

    if node.issuer.subject_alternative_names:
        builder = builder.add_extension(
            x509.IssuerAlternativeName(node.issuer.subject_alternative_names),
            critical=False,
        )

    if ocsp_url:
        builder = builder.add_extension(
            x509.AuthorityInformationAccess(
                (
                    x509.AccessDescription(
                        access_method=x509.OID_OCSP,
                        access_location=x509.UniformResourceIdentifier(ocsp_url),
                    ),
                )
            ),
            critical=False,
        )

    return builder.sign(
        private_key=node.issuer.private_key, algorithm=signature_hash_algorithm
    )


def make_id_end_entity_certificate(
    node: ChildNodeInfo,
    valid_days: int,
    signature_hash_algorithm: SignatureHashAlgorithm = hashes.SHA512(),
    ocsp_url: str | None = None,
) -> x509.Certificate:
    builder = (
        x509.CertificateBuilder()
        .subject_name(node.name)
        .issuer_name(node.issuer.name)
        .not_valid_before(now := datetime.datetime.now(datetime.UTC))
        .not_valid_after(now + datetime.timedelta(days=valid_days))
        .serial_number(x509.random_serial_number())
        .public_key(public_key := node.private_key.public_key())
        .add_extension(x509.BasicConstraints(ca=False, path_length=None), critical=True)
        .add_extension(
            x509.KeyUsage(
                digital_signature=True,
                content_commitment=True,
                key_encipherment=True,
                data_encipherment=False,
                key_agreement=False,
                key_cert_sign=False,
                crl_sign=False,
                encipher_only=False,
                decipher_only=False,
            ),
            critical=True,
        )
        .add_extension(
            x509.ExtendedKeyUsage(
                (
                    x509.OID_CLIENT_AUTH,
                    x509.OID_EMAIL_PROTECTION,
                    x509.OID_CODE_SIGNING,
                    x509.OID_TIME_STAMPING,
                )
            ),
            critical=True,
        )
        .add_extension(
            x509.SubjectKeyIdentifier.from_public_key(public_key),
            critical=False,
        )
        .add_extension(
            x509.AuthorityKeyIdentifier.from_issuer_public_key(
                node.issuer.private_key.public_key()
            ),
            critical=False,
        )
    )

    if node.subject_alternative_names:
        builder = builder.add_extension(
            x509.SubjectAlternativeName(node.subject_alternative_names),
            critical=False,
        )

    if node.issuer.subject_alternative_names:
        builder = builder.add_extension(
            x509.IssuerAlternativeName(node.issuer.subject_alternative_names),
            critical=False,
        )

    if ocsp_url:
        builder = builder.add_extension(
            x509.AuthorityInformationAccess(
                (
                    x509.AccessDescription(
                        access_method=x509.OID_OCSP,
                        access_location=x509.UniformResourceIdentifier(ocsp_url),
                    ),
                )
            ),
            critical=False,
        )

    return builder.sign(
        private_key=node.issuer.private_key, algorithm=signature_hash_algorithm
    )
