# `certoriari` - Certificate Authority manager for home networks
# Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import annotations

import datetime
from abc import ABCMeta, abstractmethod
from collections.abc import Generator
from dataclasses import dataclass, field, fields
from typing import Generic, Self, TypeVar

from cryptography import x509
from cryptography.x509 import oid

from certoriari.issuance.certificates import CertificateBlueprint, RootCACertificate
from certoriari.issuance.keys import ECDSAKey, KeyBlueprint, PrivateKey
from certoriari.issuance.links import ChildNodeInfo, NodeInfo, RootNodeInfo
from certoriari.issuance.x509.names import DirectoryName, GeneralName


@dataclass(kw_only=True)
class Org:
    country_name: str | None = None
    state_or_province_name: str | None = None
    locality_name: str | None = None
    organization_name: str


@dataclass
class NodeVersion:
    major: int = field(default_factory=lambda: datetime.datetime.today().year)
    minor: int = 1

    def __str__(self) -> str:
        return f"{self.major}.{self.minor}"

    def __format__(self, __format_spec: str | None = None) -> str:
        return str(self)

    def next(self) -> Self:
        if self.major != (current_year := datetime.datetime.today().year):
            self.major = current_year
            self.minor = 0
        else:
            self.minor += 1

        return self


NodeInfoT = TypeVar("NodeInfoT", bound=NodeInfo, covariant=True)


@dataclass
class Node(Generic[NodeInfoT], metaclass=ABCMeta):
    org: Org
    common_name: str

    organizational_unit_names: list[str] = field(default_factory=list, kw_only=True)
    subject_alternative_names: list[GeneralName] = field(
        default_factory=list, kw_only=True
    )
    version: NodeVersion = field(default_factory=NodeVersion, kw_only=True)

    private_key_blueprint: KeyBlueprint = field(default_factory=ECDSAKey, kw_only=True)
    certificate_blueprint: CertificateBlueprint[NodeInfoT] = field(kw_only=True)

    private_key: PrivateKey = field(repr=False, init=False)
    certificate: x509.Certificate = field(repr=False, init=False)

    def __post_init__(self) -> None:
        self.regenerate_private_key()
        self.regenerate_certificate()

    @property
    @abstractmethod
    def node_info(self) -> NodeInfo: ...

    @property
    def x509_name(self) -> x509.Name:
        name_attributes: list[x509.NameAttribute] = []

        for field_name in fields(self.org):
            field_value: str | None = getattr(self.org, field_name.name)
            if field_value is None:
                continue
            field_name_oid = getattr(oid.NameOID, field_name.name.upper())
            name_attributes.append(x509.NameAttribute(field_name_oid, field_value))

        for ou in self.organizational_unit_names:
            name_attributes.append(
                x509.NameAttribute(oid.NameOID.ORGANIZATIONAL_UNIT_NAME, ou)
            )

        name_attributes.append(
            x509.NameAttribute(
                oid.NameOID.COMMON_NAME,
                f"{self.org.organization_name} {self.common_name} {self.version}",
            )
        )

        return x509.Name(name_attributes)

    @property
    def x509_subject_alternative_names(self) -> list[x509.GeneralName]:
        return [
            name.x509_general_name
            for name in [DirectoryName(self.x509_name)] + self.subject_alternative_names
        ]

    def regenerate_private_key(self) -> None:
        self.private_key = self.private_key_blueprint.make_key()

    @abstractmethod
    def regenerate_certificate(self) -> None: ...


@dataclass
class RootNode(Node[RootNodeInfo]):
    certificate_blueprint: CertificateBlueprint[RootNodeInfo] = field(
        kw_only=True, default_factory=RootCACertificate
    )

    @property
    def node_info(self) -> RootNodeInfo:
        return RootNodeInfo(
            name=self.x509_name,
            private_key=self.private_key,
            subject_alternative_names=self.x509_subject_alternative_names,
        )

    def regenerate_certificate(self) -> None:
        self.certificate = self.certificate_blueprint.make_certificate(self.node_info)

    def chain(self) -> Generator[RootNode]:
        yield self


@dataclass
class ChildNode(Node[ChildNodeInfo]):
    issuer: Node[NodeInfo]

    certificate_blueprint: CertificateBlueprint[ChildNodeInfo] = field(kw_only=True)

    @property
    def node_info(self) -> ChildNodeInfo:
        return ChildNodeInfo(
            name=self.x509_name,
            private_key=self.private_key,
            subject_alternative_names=self.x509_subject_alternative_names,
            issuer=self.issuer.node_info,
        )

    def regenerate_certificate(self) -> None:
        self.certificate = self.certificate_blueprint.make_certificate(self.node_info)

    def chain(self) -> Generator[ChildNode | RootNode]:
        yield self
        yield from self.parents()

    def parents(self) -> Generator[ChildNode | RootNode]:
        current = self
        while parent := getattr(current, "issuer", None):
            yield parent
            current = parent
