# `certoriari` - Certificate Authority manager for home networks
# Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from abc import ABCMeta, abstractmethod
from dataclasses import dataclass
from typing import Generic, TypeVar

from cryptography import x509
from cryptography.hazmat.primitives import hashes

from certoriari.issuance.links import ChildNodeInfo, NodeInfo, RootNodeInfo
from certoriari.issuance.x509.certificates import (
    SignatureHashAlgorithm,
    make_crl_signer_certificate,
    make_id_end_entity_certificate,
    make_intermediary_ca_certificate,
    make_ocsp_signer_certificate,
    make_root_ca_certificate,
    make_server_end_entity_certificate,
)

NodeInfoT = TypeVar("NodeInfoT", bound=NodeInfo)


@dataclass(kw_only=True)
class CertificateBlueprint(Generic[NodeInfoT], metaclass=ABCMeta):
    valid_days: int = 365
    signature_hash_algorithm: SignatureHashAlgorithm = hashes.SHA512()

    @abstractmethod
    def make_certificate(self, node: NodeInfoT) -> x509.Certificate: ...


class RootCACertificate(CertificateBlueprint[RootNodeInfo]):
    def make_certificate(self, node: RootNodeInfo) -> x509.Certificate:
        return make_root_ca_certificate(
            node,
            valid_days=self.valid_days,
            signature_hash_algorithm=self.signature_hash_algorithm,
        )


@dataclass
class IntermediaryCACertificate(CertificateBlueprint[ChildNodeInfo]):
    path_length: int | None = None
    ocsp_url: str | None = None

    def make_certificate(self, node: ChildNodeInfo) -> x509.Certificate:
        return make_intermediary_ca_certificate(
            node,
            valid_days=self.valid_days,
            signature_hash_algorithm=self.signature_hash_algorithm,
            ocsp_url=self.ocsp_url,
            path_length=self.path_length,
        )


@dataclass
class CRLSignerCertificate(CertificateBlueprint[ChildNodeInfo]):
    ocsp_url: str | None = None

    def make_certificate(self, node: ChildNodeInfo) -> x509.Certificate:
        return make_crl_signer_certificate(
            node,
            valid_days=self.valid_days,
            signature_hash_algorithm=self.signature_hash_algorithm,
        )


class OCSPSignerCertificate(CertificateBlueprint[ChildNodeInfo]):
    def make_certificate(self, node: ChildNodeInfo) -> x509.Certificate:
        return make_ocsp_signer_certificate(
            node,
            valid_days=self.valid_days,
            signature_hash_algorithm=self.signature_hash_algorithm,
        )


@dataclass
class ServerEndEntityCertificate(CertificateBlueprint[ChildNodeInfo]):
    ocsp_url: str | None = None

    def make_certificate(self, node: ChildNodeInfo) -> x509.Certificate:
        return make_server_end_entity_certificate(
            node,
            valid_days=self.valid_days,
            signature_hash_algorithm=self.signature_hash_algorithm,
            ocsp_url=self.ocsp_url,
        )


@dataclass
class IDEndEntityCertificate(CertificateBlueprint[ChildNodeInfo]):
    ocsp_url: str | None = None

    def make_certificate(self, node: ChildNodeInfo) -> x509.Certificate:
        return make_id_end_entity_certificate(
            node,
            valid_days=self.valid_days,
            signature_hash_algorithm=self.signature_hash_algorithm,
            ocsp_url=self.ocsp_url,
        )
