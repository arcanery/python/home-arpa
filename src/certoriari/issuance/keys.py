# `certoriari` - Certificate Authority manager for home networks
# Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from abc import ABCMeta, abstractmethod
from dataclasses import dataclass

from cryptography.hazmat.primitives.asymmetric import ec, rsa

PrivateKey = ec.EllipticCurvePrivateKey | rsa.RSAPrivateKey


class KeyBlueprint(metaclass=ABCMeta):
    @abstractmethod
    def make_key(self) -> PrivateKey: ...


@dataclass
class ECDSAKey(KeyBlueprint):
    curve: ec.EllipticCurve = ec.SECP256R1()

    def make_key(self) -> ec.EllipticCurvePrivateKey:
        return ec.generate_private_key(curve=self.curve)


@dataclass
class RSAKey(KeyBlueprint):
    public_exponent: int = 65537
    key_size: int = 2048

    def make_key(self) -> rsa.RSAPrivateKey:
        return rsa.generate_private_key(
            public_exponent=self.public_exponent,
            key_size=self.key_size,
        )
