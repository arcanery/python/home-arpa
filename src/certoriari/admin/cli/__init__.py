# `certoriari` - Certificate Authority manager for home networks
# Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from cryptography.hazmat.primitives import serialization

from certoriari.issuance.certificates import (
    CRLSignerCertificate,
    IDEndEntityCertificate,
    IntermediaryCACertificate,
    OCSPSignerCertificate,
    RootCACertificate,
    ServerEndEntityCertificate,
)
from certoriari.issuance.org import ChildNode, Org, RootNode
from certoriari.issuance.x509.names import (
    DNSName,
    RFC822Name,
    UniformResourceIdentifier,
)


def app() -> None:
    org = Org(
        country_name="XX",
        state_or_province_name="Bolandia",
        locality_name="Warsaw",
        organization_name="People's Republic of Bolzga",
    )

    # Root CA

    root_ca = RootNode(
        org,
        "Root of Trust CA",
        certificate_blueprint=RootCACertificate(valid_days=365 * 20),
        organizational_unit_names=["Security"],
    )

    # Main Security Zone

    main_security_ca = ChildNode(
        org,
        "Main Security Zone Intermediary CA",
        root_ca,
        certificate_blueprint=IntermediaryCACertificate(
            valid_days=365 * 20,
            ocsp_url="https://ocsp.security.home.arpa",
        ),
        organizational_unit_names=["Security"],
        subject_alternative_names=[DNSName("security.home.arpa")],
    )
    main_ocsp_signer = ChildNode(
        org,
        "OCSP Signer",
        main_security_ca,
        certificate_blueprint=OCSPSignerCertificate(valid_days=30),
        organizational_unit_names=["Security"],
        subject_alternative_names=[DNSName("ocsp.security.home.arpa")],
    )
    main_ocsp_server = ChildNode(
        org,
        "OCSP Server",
        main_security_ca,
        certificate_blueprint=ServerEndEntityCertificate(
            valid_days=365 * 3, ocsp_url="https://ocsp.security.home.arpa"
        ),
        organizational_unit_names=["Security"],
        subject_alternative_names=[
            DNSName("ocsp.security.home.arpa"),
            UniformResourceIdentifier("https://ocsp.security.home.arpa"),
        ],
    )
    main_crl_signer = ChildNode(
        org,
        "CRL Signer",
        main_security_ca,
        certificate_blueprint=CRLSignerCertificate(valid_days=365 * 5),
        organizational_unit_names=["Security"],
        subject_alternative_names=[DNSName("crl.security.home.arpa")],
    )
    main_id_ca = ChildNode(
        org,
        "Identity Issuance Intermediary CA",
        main_security_ca,
        certificate_blueprint=IntermediaryCACertificate(
            valid_days=365 * 5,
            path_length=0,
            ocsp_url="https://ocsp.security.home.arpa",
        ),
        organizational_unit_names=["Security", "Users"],
        subject_alternative_names=[
            DNSName("id.home.arpa"),
            UniformResourceIdentifier("https://id.home.arpa"),
        ],
    )
    main_id_client = ChildNode(
        org,
        "turek@id.home.arpa",
        main_id_ca,
        certificate_blueprint=IDEndEntityCertificate(
            valid_days=365 * 5,
            ocsp_url="https://ocsp.security.home.arpa",
        ),
        organizational_unit_names=["Users"],
        subject_alternative_names=[
            RFC822Name("turek@id.home.arpa"),
            UniformResourceIdentifier("mailto:turek@id.home.arpa"),
        ],
    )
    main_issuance_ca = ChildNode(
        org,
        "Network Issuance Intermediary CA",
        main_security_ca,
        certificate_blueprint=IntermediaryCACertificate(
            valid_days=365 * 5,
            path_length=0,
            ocsp_url="https://ocsp.security.home.arpa",
        ),
        organizational_unit_names=["Security", "Network"],
        subject_alternative_names=[DNSName("issuance.security.home.arpa")],
    )
    main_router_server = ChildNode(
        org,
        "router.home.arpa",
        main_issuance_ca,
        certificate_blueprint=ServerEndEntityCertificate(
            valid_days=365 * 3, ocsp_url="https://ocsp.security.home.arpa"
        ),
        organizational_unit_names=["Network"],
        subject_alternative_names=[
            DNSName("router.home.arpa"),
            DNSName("dns.home.arpa"),
            DNSName("dhcp.home.arpa"),
            DNSName("ntp.home.arpa"),
            UniformResourceIdentifier("https://router.home.arpa"),
        ],
    )
    main_relay0_server = ChildNode(
        org,
        "relay0.home.arpa",
        main_issuance_ca,
        certificate_blueprint=ServerEndEntityCertificate(
            valid_days=365 * 3, ocsp_url="https://ocsp.security.home.arpa"
        ),
        organizational_unit_names=["Network"],
        subject_alternative_names=[
            DNSName("relay0.home.arpa"),
            UniformResourceIdentifier("https://relay0.home.arpa"),
        ],
    )
    main_kubernetes_ca = ChildNode(
        org,
        "Kubernetes Issuance Intermediary CA",
        main_security_ca,
        certificate_blueprint=IntermediaryCACertificate(
            valid_days=365 * 3,
            ocsp_url="https://ocsp.security.home.arpa",
        ),
        organizational_unit_names=["Security", "Kubernetes"],
        subject_alternative_names=[DNSName("kubernetes.home.arpa")],
    )

    # Secondary Security Zone

    secondary_security_ca = ChildNode(
        org,
        "Secondary Security Zone Intermediary CA",
        root_ca,
        certificate_blueprint=IntermediaryCACertificate(
            valid_days=365 * 20,
            ocsp_url="https://ocsp.trust.home.arpa",
        ),
        organizational_unit_names=["Security"],
        subject_alternative_names=[DNSName("trust.home.arpa")],
    )
    secondary_ocsp_signer = ChildNode(
        org,
        "OCSP Signer",
        secondary_security_ca,
        certificate_blueprint=OCSPSignerCertificate(valid_days=30),
        organizational_unit_names=["Security"],
        subject_alternative_names=[DNSName("ocsp.trust.home.arpa")],
    )
    secondary_ocsp_server = ChildNode(
        org,
        "OCSP Server",
        secondary_security_ca,
        certificate_blueprint=ServerEndEntityCertificate(
            valid_days=365 * 3, ocsp_url="https://ocsp.trust.home.arpa"
        ),
        organizational_unit_names=["Security"],
        subject_alternative_names=[
            DNSName("ocsp.trust.home.arpa"),
            UniformResourceIdentifier("https://ocsp.trust.home.arpa"),
        ],
    )
    secondary_crl_signer = ChildNode(
        org,
        "CRL Signer",
        secondary_security_ca,
        certificate_blueprint=CRLSignerCertificate(valid_days=365 * 5),
        organizational_unit_names=["Security"],
        subject_alternative_names=[DNSName("crl.trust.home.arpa")],
    )
    secondary_issuance_ca = ChildNode(
        org,
        "Network Issuance Intermediary CA",
        secondary_security_ca,
        certificate_blueprint=IntermediaryCACertificate(
            valid_days=365 * 5,
            path_length=0,
            ocsp_url="https://ocsp.trust.home.arpa",
        ),
        organizational_unit_names=["Security", "Network"],
        subject_alternative_names=[DNSName("issuance.trust.home.arpa")],
    )

    nodes = {
        # 1 CA root
        "root.ca": root_ca,
        # 1.1 Main Security Zone
        "main.security.ca": main_security_ca,
        # 1.1.1 Main Security Zone OCSP Signer
        # 1.1.2 Main Security Zone OCSP Server
        "main.ocsp.signer": main_ocsp_signer,
        "main.ocsp.server": main_ocsp_server,
        # 1.1.3 Main Security Zone CRL Signer
        "main.crl.signer": main_crl_signer,
        # 1.1.4 Main Security Zone Identity Issuance CA
        "main.id.ca": main_id_ca,
        # 1.1.4.1 turek@id.home.arpa
        "main.id.client": main_id_client,
        # 1.1.5 Main Security Zone Network Issuance CA
        "main.issuance.ca": main_issuance_ca,
        # 1.1.5.1 router.home.arpa
        "main.router.server": main_router_server,
        # 1.1.5.2 relay0.home.arpa
        "main.relay0.server": main_relay0_server,
        # 1.1.6 Main Security Zone Kubernetes Issuance CA
        "main.kubernetes.ca": main_kubernetes_ca,
        # 1.2 Secondary Security Zone
        "secondary.security.ca": secondary_security_ca,
        # 1.2.1 Secondary Security Zone Internal Services
        "secondary.ocsp.signer": secondary_ocsp_signer,
        "secondary.ocsp.server": secondary_ocsp_server,
        "secondary.crl.signer": secondary_crl_signer,
        # 1.2.2 Secondary Security Zone Network Issuance CA
        "secondary.issuance.ca": secondary_issuance_ca,
    }

    for file_name, node in nodes.items():
        with open(f"dist/{file_name}.chain.crt", "wb") as cert_file:
            for link in node.chain():
                cert_file.write(
                    link.certificate.public_bytes(
                        encoding=serialization.Encoding.PEM,
                    )
                )
        # with open(f"dist/{file_name}.key", "wb") as key_file:
        #     key_file.write(
        #         node.private_key.private_bytes(
        #             encoding=serialization.Encoding.PEM,
        #             format=serialization.PrivateFormat.TraditionalOpenSSL,
        #             encryption_algorithm=serialization.NoEncryption(),
        #         )
        #     )
