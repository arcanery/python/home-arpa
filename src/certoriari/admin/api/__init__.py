# `certoriari` - Certificate Authority manager for home networks
# Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from importlib.metadata import version

from fastapi import FastAPI
from fastapi.routing import APIRoute

from certoriari.admin.api.responses import NoContentResponse


def healthcheck() -> NoContentResponse:
    return NoContentResponse()


def app() -> FastAPI:
    return FastAPI(
        title="certoriari",
        summary="Certificate Authority manager for home networks",
        version=version("certoriari"),
        routes=[
            APIRoute("/healthz", healthcheck, response_class=NoContentResponse),
        ],
    )
