<!-- `certoriari` - Certificate Authority manager for home networks
Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

# certoriari

[![pipeline status](https://gitlab.com/arcanery/certoriari/badges/main/pipeline.svg)](https://gitlab.com/arcanery/certoriari/-/commits/main)
[![coverage report](https://gitlab.com/arcanery/certoriari/badges/main/coverage.svg)](https://gitlab.com/arcanery/certoriari/-/commits/main)
[![latest release](https://gitlab.com/arcanery/certoriari/-/badges/release.svg)](https://gitlab.com/arcanery/certoriari/-/releases)
[![security: bandit](https://img.shields.io/badge/security-bandit-green.svg)](https://github.com/PyCQA/bandit)

## Introduction

> *we wish to be made more certain...* - beginning of a writ of certoriari

`certoriari` is a friendly Certificate Authority manager for your home or other small networks.

`certoriari` allows for easy creation of multilayered CA infrastructures with intermediate
certificates and helps manage related concepts, such as OCSP.

***Importing your own root CA certificate as trusted on devices has security
implications; make sure you're aware of the risks before you do that!***

## Demo

Run the module to spit out a sample chain of certificates and their corresponding private keys.

The sample domain is `home.arpa`. RFC 8375 designates `home.arpa` as THE domain
for residential home networks, which is one of the main use cases for `certoriari`.

```bash
python -m certoriari.admin.cli
```

## Development

All development for this project happens on [GitLab](https://gitlab.com/arcanery/certoriari).

### Virtual environment

Create a new virtual environment and activate it:

```bash
python3.12 -m venv .venv
source .venv/bin/activate
python -m pip install --upgrade pip
```

If you have a preferred way of managing your virtual environments, feel free to substitute it.

### Install `poetry`

```bash
python -m pip install poetry
```

### Install the package locally

```bash
python -m poetry install --sync --all-extras --with dev --with test --with lint --with docs
```

## Testing

Run tests and generate a HTML coverage report:

```bash
python -m coverage run -m pytest --junitxml=junit.xml --verbosity=1 tests/ && python -m coverage html
```

## Documentation

Run the documentation development server:

```bash
python -m mkdocs serve --dev-addr 127.0.0.1:18000
```

# Next steps

See the [documentation](https://arcanery.gitlab.io/certoriari/) for more examples
of advanced usage and instructions for setting up a development environment.
